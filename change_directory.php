<?php
    class Path {
        private $currentPath;

        public function __construct($newDir)
        {
            $this->currentPath = $newDir;
        }

        public function cd($moveTo)
        {
            $first = substr($moveTo, 0, 1);
            $points = substr($moveTo, 0, 3);

            if ($moveTo == '/') {
                //we just show the initial Path
                $newPath = '/';
                $this->currentPath = $newPath;
                return $this->currentPath;
            } elseif ($points == '../') {
                $array_path = explode('/', trim($this->currentPath, '/'));
                array_pop($array_path); //we remove here the last element
                $newPath = implode('/', $array_path);
                $moveToFinal = substr($moveTo, 3, strlen($moveTo));
                $newPath = '/' . $newPath . '/' . $moveToFinal . '/';
                $this->currentPath = $newPath;
                return $this->currentPath;
            } elseif ($first == '/') {
                $newPath = $moveTo;
                $newPath = '/' . trim($newPath, '/') . '/';
                $this->currentPath = $newPath;
                return $this->currentPath;
            } else {
                $newPath = '/' . trim($this->currentPath, '/') . '/' . trim($moveTo, '/') . '/';
                $this->currentPath = $newPath;
                return $this->currentPath;
            }
        }

        public function setPath($newPath)
        {
            $this->currentPath = $newPath;
        }
    }

    $path = new Path('/a/b/cs/dd/');
    echo $path->cd('../oksd');
    echo '<br>';
    echo $path->cd('../sss');
    echo '<br>';
    echo $path->cd('../ok/s/d');
    echo '<br>';
    echo $path->cd('ok/s/d');
    echo '<br>';
    echo $path->cd('/s/dsdsadsadasd');
?>