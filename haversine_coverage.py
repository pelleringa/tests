#!/usr/bin/env python3
import math
locations = [
	{'id': 1000, 'zip_code': '37069', 'lat': 45.35, 'lng': 10.84},
	{'id': 1001, 'zip_code': '37121', 'lat': 45.44, 'lng': 10.99},
	{'id': 1001, 'zip_code': '37129', 'lat': 45.44, 'lng': 11.00},
	{'id': 1001, 'zip_code': '37133', 'lat': 45.43, 'lng': 11.02},
];

shoppers = [
    {'id': 'S1', 'lat': 45.46, 'lng': 11.03, 'enabled': 'true'},
    {'id': 'S2', 'lat': 45.46, 'lng': 10.12, 'enabled': 'true'},
    {'id': 'S3', 'lat': 45.34, 'lng': 10.81, 'enabled': 'true'},
    {'id': 'S4', 'lat': 45.76, 'lng': 10.57, 'enabled': 'true'},
    {'id': 'S5', 'lat': 45.34, 'lng': 10.63, 'enabled': 'true'},
    {'id': 'S6', 'lat': 45.42, 'lng': 10.81, 'enabled': 'true'},
    {'id': 'S7', 'lat': 45.34, 'lng': 10.94, 'enabled': 'true'},
];

sorted = []

R = 6373.0


def haversine(lat1, lng1, lat2, lng2):
	lat1 = math.radians(lat1)
	lng1 = math.radians(lng1)
	lat2 = math.radians(lat2)
	lng2 = math.radians(lng2)
	dlng = lng2 - lng1
	dlat = lat2 - lat1

	#Haversine formula
	a = math.sin(dlat / 2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlng / 2)**2

	c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
	distance = R * c

	return distance

for shopper in shoppers:
	shopper_data = {'shopper_id' : 0, 'coverage' : 0}
	shopper_data['shopper_id'] = shopper['id']
	for location in locations:
		if shopper['enabled'] == 'true':
			distance = haversine(shopper['lat'], shopper['lng'], location['lat'], location['lng'])
			if distance < 10:
				shopper_data['coverage'] = shopper_data['coverage'] + 1
	sorted.append(shopper_data)

def sort_shoppers(coverage):
    return coverage['coverage']

sorted.sort(key=sort_shoppers, reverse=True)

total_locations = len(locations)

final_result = []

for shopper in sorted:
	shopper['coverage'] = float(shopper['coverage']) / float(total_locations) * 100
	final_result.append(shopper)

print('Final_result:', final_result)