function toBinaryAndReverse(num) {
    var binaryNum = [];
    var initialNum = num;
    while (num > 0) {
     binaryNum.push(num % 2);
     
     num = parseInt(num / 2);
     console.log('binaryNum', binaryNum);
     console.log('num', num);
    }
    //we don't need to reverse the result because to get the binary from num we would need to reverse the mod results, so is not needed to reverse and reverse the reversed
    console.log('the reversed binary of ' + initialNum + ' is ' + binaryNum);
   
    //now we have to reverse it to calculate the potence of each position from left to right instead of right to left as it should be
   
    binaryNum = binaryNum.reverse();
    //console.log('reversed', binaryNum);
   
    var newNum = 0;
   
    for (var key in binaryNum) {
     if (binaryNum[key] == 1) {
      newNum += Math.pow(2, key);
     }
    }
    console.log('new num is ' + newNum);
    document.write("new num is " + newNum);
}

toBinaryAndReverse(13);